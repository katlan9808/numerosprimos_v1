﻿using System;

namespace NumerosPrimos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite un numero: ");
            string number = Console.ReadLine();
            int numberValid;
            bool numberOk = int.TryParse(number, out numberValid);
            while (!numberOk)
            {
                Console.WriteLine("Digite un numero: ");
                number = Console.ReadLine();
                numberOk = int.TryParse(number, out numberValid);
            }

            Console.WriteLine(PrimeNumbers.IsPrime(numberValid));
            Console.ReadKey();

        }
    }
}
