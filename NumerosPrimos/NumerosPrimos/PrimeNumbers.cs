﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NumerosPrimos
{
    public static class PrimeNumbers
    {
        public static bool IsPrime(int numero)
        {
            for (int i = 2; i < numero; i++)
            {
                if ((numero % i) == 0)
                {
                    return false;
                }
            }
             return true;
        }
    }
}
